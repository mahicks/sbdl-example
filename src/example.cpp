#include <iostream>

// optional: allows for more syntactically appealing SBDL annotation
#define _sbdl(...)

// C++ implementation of architectural description
_sbdl([@MKID:@FILE] is realisation { [@DFP] definition is arch_doc })

//-----------------------------------------------------------------------------

class External_System {
  _sbdl(using { aspect is External_System })
  public:
    int method1() {
      _sbdl([@MKID:@CFUNC] is event { description is "[@CPPCLASS]::[@CFUNC]" output is "some data" })
      return 1;
    }

    int method2() {
      _sbdl([@MKID:@CFUNC] is event { description is "[@CPPCLASS]::[@CFUNC]" output is "more data" })
      return 2;
    }

    int method3() {
      _sbdl([@MKID:@CFUNC] is event { description is "[@CPPCLASS]::[@CFUNC]" output is "even more data" })
      return 3;
    }

    int method4() {
      _sbdl([@MKID:@CFUNC] is event { description is "[@CPPCLASS]::[@CFUNC]" output is "last bit of data" })
      return 4;
    }
};

//-----------------------------------------------------------------------------

class Example_Class {
  _sbdl(using { aspect is Example_Item })
  private:
    External_System ext;
  public:
    int prod_method() {
      _sbdl([@MKID:@CFUNC] is function { description is "[@CPPCLASS]::[@CFUNC]" event is method1,method2,method2,method3,method4 })
      return ext.method1() * ext.method2() * ext.method3() * ext.method4();
    }
    int sum_method() {
      _sbdl([@MKID:@CFUNC] is function { description is "[@CPPCLASS]::[@CFUNC]" event is method1,method2,method2,method3,method4 })
      return ext.method1() + ext.method2() + ext.method3() + ext.method4();
    }
    int count_method(int count) {
      int countl;
      for (countl = 0; countl < count; countl++) {};
      return countl;
    }
    void fail (bool do_fail)
    {
      if (do_fail) {
        _sbdl(scenario_a is fmea:cause { description is "Embedding failure identifiers as error codes is useful for traceability ([@CPPCLASS]::[@CFUNC])" })
        throw std::runtime_error("@sbdl ev1 is trace { fmea:cause is scenario_a }#");
      }
    }
};

//-----------------------------------------------------------------------------
#ifndef NOMAIN

int main() {
  Example_Class ex;
  ex.fail(false);
  std::cout << "      A number:" << ex.prod_method() << std::endl;
  std::cout << "Another number:" << ex.sum_method()  << std::endl;
}

#endif
//-----------------------------------------------------------------------------
