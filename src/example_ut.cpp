#include <iostream>
#include <functional>

#define NOMAIN
#include "example.cpp"

// This file uses the standard sbdl annotation syntax (// '@'sbdl)

typedef int ut_func();

void execute_ut(std::string ut_name, ut_func ut, int *failure_count, int *exec_count) {
  std::cout << "UT: " << ut_name << ", ";
  auto result = ut();
  auto result_string = result == 0 ? "PASS" : "FAIL";
  (*failure_count) += result;
  (*exec_count)++;
  std::cout << "RESULT: " << result_string << std::endl;
}

int test_prodsum() {
  //@sbdl [@MKID:@CFUNC] is test { description is "Parametrically test whether products and summations look correct" }
  int result = 0;
  Example_Class ex;
  result = (ex.prod_method() >= ex.sum_method()) ? 0 : 1;
  return result;
}

int test_fail() {
  //@sbdl [@MKID:@CFUNC] is test { description is "Test whether a failing function behaves as expected" fmea:cause is scenario_a }
  int result = 1;
  Example_Class ex;
  try {
    ex.fail(true);
  }
  catch(std::exception &e) {
    result = 0;
    std::cerr << std::endl << "ERROR: " << e.what() << std::endl;
  }
  return result;
}

int test_count() {
  //@sbdl [@MKID:@CFUNC] is test { description is "Parametrically test whether counting looks correct" }
  int result = 0;
  Example_Class ex;
  result = (ex.count_method(100000)==100000) ? 0 : 1;
  return result;
}

int main() {
  int failure_count  = 0;
  int executed_count = 0;
  using namespace std::placeholders;
  auto execute_ut_l = std::bind(execute_ut, _1, _2, &failure_count, &executed_count);
  execute_ut_l("Test Product/Summation",test_prodsum);
  execute_ut_l("Test Test Failing     ",test_fail);
  execute_ut_l("Test Counting         ",test_count);
  std::cout << "-------\n"
            << "EXECUTED: " << executed_count << ", "
            << "PASSED: "   << (executed_count - failure_count) << ", "
            << "FAILED: "   << failure_count
            << std::endl;
  return failure_count;
}
