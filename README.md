# SBDL Example

## Purpose

This directory contains a small set of files, arranged as a miniature example
software project, to demonstrate how SBDL can be used and distributed among
different materials (documents, source ...) to keep system information in
spatial proximity with its relevant realistion.

This example does not aim to:

* Provide realistic system/program content
* Provide realistic failure modes

## How to Build

You can build the project using CMake. This will build the example (trivial)
program and, more important, the SBDL and FMEA output.

> cmake -B build

> cmake --build build

Output:

1. example (executable)
2. example.sbdl (aggregated SBDL)
3. example_requirements_model.png (Sys UML Requirements) [Requires PlantUML]
4. example_architecture_model.png (UML Model) [Requires PlantUML]
5. example_failure_model.png (NetworkX Model)
6. example.fmea (OpenFMEA) [Can be opened with FMEA.dev]

You can view the latest built output for the above steps [here](https://sbdl.dev/example).
And the generated document output [here](https://sbdl.dev/example/Generated_Document.html).

## Where are the SBDL definitions?

For demonstration purposes, SBDL entries are distributed throughout the
example materials.

Word Document
: Aspects, Failure Effects, Failure Controls

Excel Document
: Requirements, Failure Modes

CPP Source Code
: Failure Causes (including assumptions and dependencies), Failure Controls, Design Actions

CPP Unit Tests
: Failure Detections, Detection Actions

MarkDown
: @sbdl mdcontrol is fmea:control { description is "We can easily embed control information in Markdown" }

## Contact

Web
: [sbdl.dev](https://sbdl.dev)

Author
: Michael A, Hicks

EMail
: contact@sbdl.dev

